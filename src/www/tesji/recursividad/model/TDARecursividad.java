package www.tesji.recursividad.model;
import java.util.Scanner;

public class TDARecursividad {
	public static void main(String[] args) {
		Scanner entrada = new Scanner(System.in);
	    TDARecursividad objEscalera = new TDARecursividad();
	    int numEscalera; 
	    
	    System.out.print("\nNumero de escalera :");
	    numEscalera = entrada.nextInt();
	    objEscalera.bajarEscalera(numEscalera);
	}
	
	public void bajarEscalera(int numEscalera) {
		if(numEscalera == 0) {
			System.out.print("\nNo hay mas escaleras por bajar.");
		}else if(numEscalera < 0){
			System.out.print("\nEl numero de escaleras no puede ser negativo");;
		}else {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.print("\nBajando escalon :" + numEscalera);
			bajarEscalera(numEscalera - 1); 
		}
	}
}
